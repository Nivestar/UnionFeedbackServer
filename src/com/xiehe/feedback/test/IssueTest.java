package com.xiehe.feedback.test;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.eclipse.jetty.server.Authentication.User;
import org.junit.Before;
import org.junit.Test;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.sun.xml.internal.fastinfoset.algorithm.HexadecimalEncodingAlgorithm;
import com.xiehe.feedback.model.Issues;
import com.xiehe.feedback.model.Users;
import com.xiehe.feedback.model._MappingKit;
import com.xiehe.feedback.util.StringUtils;

import junit.framework.TestCase;

public class IssueTest extends TestCase {
	
	private ActiveRecordPlugin arp;
	private C3p0Plugin c3p0Plugin;

	@Override
	protected void setUp() throws Exception {
		PropKit.use("config.properties");

		// 配置数据库连接池插件
		c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"));
		// orm映射 配置ActiveRecord插件
		arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setShowSql(PropKit.getBoolean("devMode"));
		arp.setDialect(new MysqlDialect());
		// ********在此添加数据库 表-Model 映射*********//
		_MappingKit.mapping(arp);
		// 添加到插件列表中
		c3p0Plugin.start();
		arp.start();
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		arp.stop();
		c3p0Plugin.stop();
		super.tearDown();
	}

	public void testAddIssue() {
		Issues issue = new Issues();
		issue.setProjectId(2); //工程：测试应用, 关联表projects
		issue.setTrackerId(8); //跟踪：信息同步,关联表trackers
		issue.setSubject("主题：Test By ZWC "); //主题
		issue.setDescription("详情 Test By ZWC"); //详情
		//issue.setDueDate(); //不知
		//issue.setCategoryId(); //分组？但是数据库中表issues_category只有一个 
		issue.setStatusId(1); //状态：初始为1,进行中，关联表 issue_statuses
		//issue.getAssignedToId(); //指派人，关联表users
		issue.setPriorityId(1); //设置优先级，没找到关联的表
		//issue.setFixedVersionId(); //不知
		issue.setAuthorId(1); //发表人，关联表users
		//issue.setLockVersion(); // 不知道干嘛的
		Date now = new Date();
		issue.setCreatedOn(now); //创建时间
		issue.setUpdatedOn(now); //更新时间，默认与创建时间相同
		issue.setStartDate(now); //issue开始时间
		issue.setDoneRatio(0); //issue完成进度百分比，默认为0
		//issue.setEstimatedHours(); //不知
		//issue.setParentId(); //不知
		//issue.setRootId(); //不知
		//issue.setLft(); //不知
		//issue.setRgt(); //不知
		issue.setIsPrivate(false); //是否私有 ，默认为0，公开
		issue.setClosedOn(null); //issue关闭时间，默认为空
		//issue.setSprintId(); //不知
		//issue.setPosition(); //应该是列表显示位置，不知道有何意义
		if (issue.save()){
			System.out.println("成功!");
		}
		
	}
	
	public void testGenerateSalt(){
		
		System.out.println(Users.generateSalt());
		
	}
	
	public void testUserLogin(){
		String login = "ZWC";
		String password = "ZWC";
		Users user = Users.findByLogin(login);
		String salt = user.getSalt();
		String result = StringUtils.SHA1( salt + StringUtils.SHA1(password) );
		
		assertEquals(user.getHashedPassword(), result);
	}
	
	
	public void testUserRegister(){
		String login = "ZWC";
		String password = "ZWC";
		
		String salt = Users.generateSalt();
		String hashedPassword = StringUtils.SHA1( salt + StringUtils.SHA1(password) );
		
		int maxId = Db.queryInt("select max(id) from users");
		Users user = new Users();
		
		user.setId(maxId + 1);
		user.setLogin(login);
		user.setHashedPassword(hashedPassword);
		user.setSalt(salt);
		user.setFirstname("张文超");
		user.setLastname("计算机管理中心");
		user.setMail("rick255@qq.com");
		user.setType("User"); //必填，不填就报错
		user.setAdmin(false);
		user.setStatus(1);
		user.setMailNotification("only_my_events");
		user.setMustChangePasswd(false);
		user.setGrantPush(true);
		user.setSoundOnOff(true);
		user.save();
		
	}
	
	
	
	

}
