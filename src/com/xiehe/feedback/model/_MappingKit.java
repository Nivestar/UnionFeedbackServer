package com.xiehe.feedback.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("agile_colors", "id", AgileColors.class);
		arp.addMapping("agile_data", "id", AgileData.class);
		arp.addMapping("attachments", "id", Attachments.class);
		arp.addMapping("auth_sources", "id", AuthSources.class);
		arp.addMapping("banners", "id", Banners.class);
		arp.addMapping("boards", "id", Boards.class);
		arp.addMapping("changes", "id", Changes.class);
		arp.addMapping("changeset_parents", "changeset_id, parent_id", ChangesetParents.class);
		arp.addMapping("changesets", "id", Changesets.class);
		arp.addMapping("changesets_issues", "changeset_id, issue_id", ChangesetsIssues.class);
		arp.addMapping("comments", "id", Comments.class);
		arp.addMapping("custom_fields", "id", CustomFields.class);
		//arp.addMapping("custom_fields_projects", "", CustomFieldsProjects.class);
		//arp.addMapping("custom_fields_roles", "", CustomFieldsRoles.class);
		//arp.addMapping("custom_fields_trackers", "", CustomFieldsTrackers.class);
		arp.addMapping("custom_values", "id", CustomValues.class);
		arp.addMapping("documents", "id", Documents.class);
		arp.addMapping("enabled_modules", "id", EnabledModules.class);
		arp.addMapping("enumerations", "id", Enumerations.class);
		//arp.addMapping("groups_users", "", GroupsUsers.class);
		arp.addMapping("issue_badge_user_settings", "id", IssueBadgeUserSettings.class);
		arp.addMapping("issue_categories", "id", IssueCategories.class);
		arp.addMapping("issue_relations", "id", IssueRelations.class);
		arp.addMapping("issue_statuses", "id", IssueStatuses.class);
		arp.addMapping("issue_todo_list_items", "id", IssueTodoListItems.class);
		arp.addMapping("issue_todo_lists", "id", IssueTodoLists.class);
		arp.addMapping("issues", "id", Issues.class);
		arp.addMapping("journal_details", "id", JournalDetails.class);
		arp.addMapping("journals", "id", Journals.class);
		arp.addMapping("kb_articles", "id", KbArticles.class);
		arp.addMapping("kb_categories", "id", KbCategories.class);
		arp.addMapping("member_roles", "id", MemberRoles.class);
		arp.addMapping("members", "id", Members.class);
		arp.addMapping("messages", "id", Messages.class);
		arp.addMapping("news", "id", News.class);
		arp.addMapping("open_id_authentication_associations", "id", OpenIdAuthenticationAssociations.class);
		arp.addMapping("open_id_authentication_nonces", "id", OpenIdAuthenticationNonces.class);
		arp.addMapping("pending_efforts", "id", PendingEfforts.class);
		arp.addMapping("projects", "id", Projects.class);
		//arp.addMapping("projects_trackers", "", ProjectsTrackers.class);
		arp.addMapping("queries", "id", Queries.class);
		//arp.addMapping("queries_roles", "", QueriesRoles.class);
		arp.addMapping("ratings", "id", Ratings.class);
		arp.addMapping("repositories", "id", Repositories.class);
		arp.addMapping("roles", "id", Roles.class);
		//arp.addMapping("schema_migrations", "", SchemaMigrations.class);
		arp.addMapping("settings", "id", Settings.class);
		arp.addMapping("sprint_efforts", "id", SprintEfforts.class);
		arp.addMapping("sprints", "id", Sprints.class);
		arp.addMapping("taggings", "id", Taggings.class);
		arp.addMapping("tags", "id", Tags.class);
		arp.addMapping("theme_changer_user_settings", "id", ThemeChangerUserSettings.class);
		arp.addMapping("time_entries", "id", TimeEntries.class);
		arp.addMapping("tokens", "id", Tokens.class);
		arp.addMapping("trackers", "id", Trackers.class);
		arp.addMapping("user_preferences", "id", UserPreferences.class);
		arp.addMapping("users", "id", Users.class);
		arp.addMapping("versions", "id", Versions.class);
		arp.addMapping("view_customizes", "id", ViewCustomizes.class);
		arp.addMapping("viewings", "id", Viewings.class);
		arp.addMapping("watchers", "id", Watchers.class);
		arp.addMapping("wiki_content_versions", "id", WikiContentVersions.class);
		arp.addMapping("wiki_contents", "id", WikiContents.class);
		arp.addMapping("wiki_pages", "id", WikiPages.class);
		arp.addMapping("wiki_redirects", "id", WikiRedirects.class);
		arp.addMapping("wikis", "id", Wikis.class);
		arp.addMapping("workflows", "id", Workflows.class);
	}
}

