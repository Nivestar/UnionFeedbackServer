package com.xiehe.feedback.model;

import com.xiehe.feedback.model.base.BaseWikiContents;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class WikiContents extends BaseWikiContents<WikiContents> {
	public static final WikiContents dao = new WikiContents();
}
