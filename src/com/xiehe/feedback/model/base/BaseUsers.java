package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUsers<M extends BaseUsers<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setLogin(java.lang.String login) {
		set("login", login);
	}

	public java.lang.String getLogin() {
		return get("login");
	}

	public void setHashedPassword(java.lang.String hashedPassword) {
		set("hashed_password", hashedPassword);
	}

	public java.lang.String getHashedPassword() {
		return get("hashed_password");
	}

	public void setFirstname(java.lang.String firstname) {
		set("firstname", firstname);
	}

	public java.lang.String getFirstname() {
		return get("firstname");
	}

	public void setLastname(java.lang.String lastname) {
		set("lastname", lastname);
	}

	public java.lang.String getLastname() {
		return get("lastname");
	}

	public void setMail(java.lang.String mail) {
		set("mail", mail);
	}

	public java.lang.String getMail() {
		return get("mail");
	}

	public void setAdmin(java.lang.Boolean admin) {
		set("admin", admin);
	}

	public java.lang.Boolean getAdmin() {
		return get("admin");
	}

	public void setStatus(java.lang.Integer status) {
		set("status", status);
	}

	public java.lang.Integer getStatus() {
		return get("status");
	}

	public void setLastLoginOn(java.util.Date lastLoginOn) {
		set("last_login_on", lastLoginOn);
	}

	public java.util.Date getLastLoginOn() {
		return get("last_login_on");
	}

	public void setLanguage(java.lang.String language) {
		set("language", language);
	}

	public java.lang.String getLanguage() {
		return get("language");
	}

	public void setAuthSourceId(java.lang.Integer authSourceId) {
		set("auth_source_id", authSourceId);
	}

	public java.lang.Integer getAuthSourceId() {
		return get("auth_source_id");
	}

	public void setCreatedOn(java.util.Date createdOn) {
		set("created_on", createdOn);
	}

	public java.util.Date getCreatedOn() {
		return get("created_on");
	}

	public void setUpdatedOn(java.util.Date updatedOn) {
		set("updated_on", updatedOn);
	}

	public java.util.Date getUpdatedOn() {
		return get("updated_on");
	}

	public void setType(java.lang.String type) {
		set("type", type);
	}

	public java.lang.String getType() {
		return get("type");
	}

	public void setIdentityUrl(java.lang.String identityUrl) {
		set("identity_url", identityUrl);
	}

	public java.lang.String getIdentityUrl() {
		return get("identity_url");
	}

	public void setMailNotification(java.lang.String mailNotification) {
		set("mail_notification", mailNotification);
	}

	public java.lang.String getMailNotification() {
		return get("mail_notification");
	}

	public void setSalt(java.lang.String salt) {
		set("salt", salt);
	}

	public java.lang.String getSalt() {
		return get("salt");
	}

	public void setMustChangePasswd(java.lang.Boolean mustChangePasswd) {
		set("must_change_passwd", mustChangePasswd);
	}

	public java.lang.Boolean getMustChangePasswd() {
		return get("must_change_passwd");
	}

	public void setPasswdChangedOn(java.util.Date passwdChangedOn) {
		set("passwd_changed_on", passwdChangedOn);
	}

	public java.util.Date getPasswdChangedOn() {
		return get("passwd_changed_on");
	}

	public void setOs(java.lang.String os) {
		set("os", os);
	}

	public java.lang.String getOs() {
		return get("os");
	}

	public void setUdid(java.lang.String udid) {
		set("udid", udid);
	}

	public java.lang.String getUdid() {
		return get("udid");
	}

	public void setMobileKey(java.lang.String mobileKey) {
		set("mobile_key", mobileKey);
	}

	public java.lang.String getMobileKey() {
		return get("mobile_key");
	}

	public void setMobileStatus(java.lang.Integer mobileStatus) {
		set("mobile_status", mobileStatus);
	}

	public java.lang.Integer getMobileStatus() {
		return get("mobile_status");
	}

	public void setMobileLastLoginOn(java.util.Date mobileLastLoginOn) {
		set("mobile_last_login_on", mobileLastLoginOn);
	}

	public java.util.Date getMobileLastLoginOn() {
		return get("mobile_last_login_on");
	}

	public void setLoginErrCount(java.lang.Integer loginErrCount) {
		set("login_err_count", loginErrCount);
	}

	public java.lang.Integer getLoginErrCount() {
		return get("login_err_count");
	}

	public void setGrantPush(java.lang.Boolean grantPush) {
		set("grant_push", grantPush);
	}

	public java.lang.Boolean getGrantPush() {
		return get("grant_push");
	}

	public void setSoundOnOff(java.lang.Boolean soundOnOff) {
		set("sound_on_off", soundOnOff);
	}

	public java.lang.Boolean getSoundOnOff() {
		return get("sound_on_off");
	}

}
