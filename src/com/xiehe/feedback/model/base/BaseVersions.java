package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseVersions<M extends BaseVersions<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setProjectId(java.lang.Integer projectId) {
		set("project_id", projectId);
	}

	public java.lang.Integer getProjectId() {
		return get("project_id");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}

	public java.lang.String getName() {
		return get("name");
	}

	public void setDescription(java.lang.String description) {
		set("description", description);
	}

	public java.lang.String getDescription() {
		return get("description");
	}

	public void setEffectiveDate(java.util.Date effectiveDate) {
		set("effective_date", effectiveDate);
	}

	public java.util.Date getEffectiveDate() {
		return get("effective_date");
	}

	public void setCreatedOn(java.util.Date createdOn) {
		set("created_on", createdOn);
	}

	public java.util.Date getCreatedOn() {
		return get("created_on");
	}

	public void setUpdatedOn(java.util.Date updatedOn) {
		set("updated_on", updatedOn);
	}

	public java.util.Date getUpdatedOn() {
		return get("updated_on");
	}

	public void setWikiPageTitle(java.lang.String wikiPageTitle) {
		set("wiki_page_title", wikiPageTitle);
	}

	public java.lang.String getWikiPageTitle() {
		return get("wiki_page_title");
	}

	public void setStatus(java.lang.String status) {
		set("status", status);
	}

	public java.lang.String getStatus() {
		return get("status");
	}

	public void setSharing(java.lang.String sharing) {
		set("sharing", sharing);
	}

	public java.lang.String getSharing() {
		return get("sharing");
	}

}
