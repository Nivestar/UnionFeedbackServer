package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseIssueTodoListItems<M extends BaseIssueTodoListItems<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setIssueTodoListId(java.lang.Integer issueTodoListId) {
		set("issue_todo_list_id", issueTodoListId);
	}

	public java.lang.Integer getIssueTodoListId() {
		return get("issue_todo_list_id");
	}

	public void setIssueId(java.lang.Integer issueId) {
		set("issue_id", issueId);
	}

	public java.lang.Integer getIssueId() {
		return get("issue_id");
	}

	public void setPosition(java.lang.Integer position) {
		set("position", position);
	}

	public java.lang.Integer getPosition() {
		return get("position");
	}

}
