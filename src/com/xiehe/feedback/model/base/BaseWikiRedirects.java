package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseWikiRedirects<M extends BaseWikiRedirects<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setWikiId(java.lang.Integer wikiId) {
		set("wiki_id", wikiId);
	}

	public java.lang.Integer getWikiId() {
		return get("wiki_id");
	}

	public void setTitle(java.lang.String title) {
		set("title", title);
	}

	public java.lang.String getTitle() {
		return get("title");
	}

	public void setRedirectsTo(java.lang.String redirectsTo) {
		set("redirects_to", redirectsTo);
	}

	public java.lang.String getRedirectsTo() {
		return get("redirects_to");
	}

	public void setCreatedOn(java.util.Date createdOn) {
		set("created_on", createdOn);
	}

	public java.util.Date getCreatedOn() {
		return get("created_on");
	}

}
