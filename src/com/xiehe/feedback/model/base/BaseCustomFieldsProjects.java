package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomFieldsProjects<M extends BaseCustomFieldsProjects<M>> extends Model<M> implements IBean {

	public void setCustomFieldId(java.lang.Integer customFieldId) {
		set("custom_field_id", customFieldId);
	}

	public java.lang.Integer getCustomFieldId() {
		return get("custom_field_id");
	}

	public void setProjectId(java.lang.Integer projectId) {
		set("project_id", projectId);
	}

	public java.lang.Integer getProjectId() {
		return get("project_id");
	}

}
