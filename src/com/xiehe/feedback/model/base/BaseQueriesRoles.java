package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseQueriesRoles<M extends BaseQueriesRoles<M>> extends Model<M> implements IBean {

	public void setQueryId(java.lang.Integer queryId) {
		set("query_id", queryId);
	}

	public java.lang.Integer getQueryId() {
		return get("query_id");
	}

	public void setRoleId(java.lang.Integer roleId) {
		set("role_id", roleId);
	}

	public java.lang.Integer getRoleId() {
		return get("role_id");
	}

}
