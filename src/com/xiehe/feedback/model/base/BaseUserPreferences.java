package com.xiehe.feedback.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUserPreferences<M extends BaseUserPreferences<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setUserId(java.lang.Integer userId) {
		set("user_id", userId);
	}

	public java.lang.Integer getUserId() {
		return get("user_id");
	}

	public void setOthers(java.lang.String others) {
		set("others", others);
	}

	public java.lang.String getOthers() {
		return get("others");
	}

	public void setHideMail(java.lang.Boolean hideMail) {
		set("hide_mail", hideMail);
	}

	public java.lang.Boolean getHideMail() {
		return get("hide_mail");
	}

	public void setTimeZone(java.lang.String timeZone) {
		set("time_zone", timeZone);
	}

	public java.lang.String getTimeZone() {
		return get("time_zone");
	}

}
