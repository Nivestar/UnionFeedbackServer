package com.xiehe.feedback.model;

import java.util.Random;

import com.xiehe.feedback.model.base.BaseUsers;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Users extends BaseUsers<Users> {
	public static final Users dao = new Users();
	
	
	public static Users findByLogin(String name){
		return Users.dao.findFirst("SELECT * FROM USERS WHERE LOGIN = ?", name);
		
	}

	public static String generateSalt() {
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < 16; i++) {
			int a = r.nextInt(0xFF);
			String tmp = Integer.toHexString(a);
			if (tmp.length() == 1)
				tmp += "0";
			sb.append(tmp);
		}
		
		return sb.toString();

	}
	
}
