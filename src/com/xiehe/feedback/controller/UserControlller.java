package com.xiehe.feedback.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.ext.interceptor.Restful;
import com.jfinal.kit.Ret;
import com.xiehe.feedback.model.Users;
import com.xiehe.feedback.util.StringUtils;

public class UserControlller extends Controller {

	@Before(POST.class)
	public void login() {
		
		
		String login = getPara("login");
		String password = getPara("password");
		Ret ret = Ret.create();
		
		Users user = Users.findByLogin(login);
		System.out.println();
		if (user == null) {
			ret.put("msg", "没有该用户名");
			ret.put("code", "-1");
		} else {
			String salt = user.getSalt();
			String loginHashedPassword = StringUtils.SHA1(salt + StringUtils.SHA1(password));
			if (!loginHashedPassword.equals(user.getHashedPassword())) {
				ret.put("msg", "密码不正确");
				ret.put("code", "-2");
			} else {
				ret.put("msg", "success");
				ret.put("code", "0");
				ret.put("userName", user.getFirstname());
				ret.put("department", user.getLastname());
			}
		}
		renderJson(ret.getData());
	}
	
	@Before(Restful.class)
	public void register(){
		
	}

}
