package com.xiehe.feedback.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.Restful;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.sun.media.sound.JavaSoundAudioClip;
import com.xiehe.feedback.model.Issues;

public class FeedbackController extends Controller {
	private static int pageSize = 10;
	
	public void trackers(){
		String sql = "SELECT DISTINCT tracker_id, trackers.name AS tracker_name, project_id, projects.name AS project_name from issues, trackers, projects";
		sql += " WHERE issues.tracker_id = trackers.id AND issues.project_id = projects.id ORDER BY project_id, tracker_id";
		List<Record> records =  Db.find(sql);
		renderJson(records);
		
	}

	public void query() {
		int pageNum = getParaToInt("pageNum");
		Page<Issues> issues = Issues.dao.paginate(pageNum, pageSize,
				"SELECT issues.*, users.firstname, users.lastname, issue_statuses.name AS status_name, projects.name AS project_name, trackers.name AS tracker_name",
				"FROM issues, users, issue_statuses, trackers, projects WHERE issues.closed_on IS NULL AND issues.author_id = users.id AND issues.status_id = issue_statuses.id AND issues.project_id = projects.id AND issues.tracker_id = trackers.id ORDER BY issues.id DESC");
		renderJson(issues);
	}

	public void queryById() {
		int issue_id = getParaToInt("issue_id");
		String sql = "SELECT issues.*, users.firstname, users.lastname, issue_statuses.name AS status_name, projects.name AS project_name, trackers.name AS tracker_name FROM issues, users, issue_statuses, trackers, projects";
		sql += " WHERE issues.id = ? AND issues.closed_on IS NULL AND issues.author_id = users.id AND issues.status_id = issue_statuses.id AND issues.project_id = projects.id AND issues.tracker_id = trackers.id";
		Issues issue = Issues.dao.findFirst(sql, issue_id);
		renderJson(issue);

	}

	public void queryNewest() {
		String newestTime = getPara("newestTime");
		String sql = "SELECT issues.*, users.firstname, users.lastname, issue_statuses.name AS status_name, projects.name AS project_name, trackers.name AS tracker_name FROM issues, users, issue_statuses, trackers, projects";
		sql += " WHERE issues.created_on > ? AND issues.closed_on IS NULL AND issues.author_id = users.id AND issues.status_id = issue_statuses.id AND issues.project_id = projects.id AND issues.tracker_id = trackers.id ORDER BY issues.id DESC";
		List<Issues> issues = Issues.dao.find(sql, newestTime);
		renderJson(issues);
		
	}
	
	public void upload() {
		List<UploadFile> files = getFiles();
		Map<String, String[]> maps = getParaMap();

		System.out.println(maps.size());

		Map m = new HashMap<String, String>();
		m.put("ret", "0");
		m.put("desc", "success");
		renderJson(m);
	}

	public void index() {
		renderText("index");
	}

	public void add() {
		renderText("add");
	}

}
