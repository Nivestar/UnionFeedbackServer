package com.xiehe.feedback.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringUtils {
	
	public static String SHA1(String str){
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			md.update(str.getBytes());
			
			byte[] tmpBytes  = md.digest();
			
			// 字节数组转换为 十六进制 数
			StringBuilder hexString = new StringBuilder();
			for (int i = 0; i < tmpBytes.length; i++) {
				String shaHex = Integer.toHexString(tmpBytes[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexString.append("0");
				}
				hexString.append(shaHex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

}
